package main

import (
	"github.com/mchampaneri/imp/modules/cache"
	"github.com/mchampaneri/imp/modules/config"
	"github.com/mchampaneri/imp/modules/database"
	"github.com/mchampaneri/imp/modules/logger"
	"github.com/mchampaneri/imp/modules/search"
	"github.com/mchampaneri/imp/modules/validator"
	"github.com/mchampaneri/imp/routes"
	// "github.com/mchampaneri/imp/modules/objectstore"
)

func init() {
	/* Bootstrapping the app  Loading the Configurations*
	|  from the config  file
	*/
	config.LoadAppConfig()

	/*
	 | Enforcing the goValidator over the models (Structs)
	*/
	validator.Init()
	/* Connecting the Database and setting up the default
	|  default parameters for the opened connection
	*/
	database.Connection = database.GetConnection()
	database.Connection.SetMaxIdleConns(100)

	/* Rendering The Views for the normal requests
	|  and json response for the ajax requests
	*/

	/* Binding the Logger Singleton to the app for the
	|  further operations
	*/
	logger.DefaultLogger = logger.Log{}

	/* Wiper uses the BigCache as its built in cache service
	|  provider . Initializing the  Cache Singleton
	*/
	cache.BigCache, _ = cache.InitCache()

	// objectstore.B2init()

	search.LoadIndex()
}

func main() {
	/* Loading the Routers for the web and api on their
	|  mentioned ports
	*/
	go func() {
		routes.RegisterApiRoutes()
	}()
	routes.RegisterWebRoutes()
}
