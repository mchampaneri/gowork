const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resource/js/app.js', 'public/js')
   .sass('resource/sass/app.scss', 'public/css');

mix.sass(
    'resource/plugins/admin/scss/AdminLTE.scss'      
    ,'public/css/admin.css');   

mix.scripts([
    'resource/plugins/adminlte/js/app.min.js',
    'resource/plugins/tag/tags.js',
    ],'public/js/admin.js');

mix.scripts([
        // Core Plugins //
        'resource/plugins/jquery/jquery.js',
        'resource/plugins/validator/jfvalidator.js',
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        'resource/plugins/noti/notify.js',
        'resource/plugins/snote/summernote.js',
        'resource/plugins/typed/typed.js',
        'resource/plugins/tag/tags.js',
        // Custom Scripts //
        'resource/plugins/puberstreet/scriptInit.js',
         // Extra Plugins//
        ], 'public/js/plugin.js');