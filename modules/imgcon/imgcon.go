package imgcon

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/mchampaneri/imp/modules/objectstore"
	"image/jpeg"
	"image/png"
	"os"
	"path/filepath"
	"strings"
)

func ConvertAndStore(image, location string) string {
	var f *os.File
	var storedat, url string
	coI := strings.Index(image, ",")
	rawImage := image[coI+1:]
	// Encoded Image DataUrl //
	unbased, _ := base64.StdEncoding.DecodeString(rawImage)
	res := bytes.NewReader(unbased)
	switch strings.TrimSuffix(image[6:coI], ";base64") {
	case "image/png":
		pngI, _ := png.Decode(res)
		f, _ = os.OpenFile(location+".png", os.O_WRONLY|os.O_CREATE, 0777)
		png.Encode(f, pngI)
		storedat = location + ".png"
		f.Close()
	case "image/jpeg":
		jpgI, _ := jpeg.Decode(res)
		f, _ = os.OpenFile(location+".jpg", os.O_WRONLY|os.O_CREATE, 0777)
		jpeg.Encode(f, jpgI, &jpeg.Options{Quality: 75})
		storedat = location + ".jpg"
		f.Close()
	}

	reader, err := os.Open(storedat)
	name := filepath.Base(storedat)
	metadata := make(map[string]string)
	if err != nil {
		fmt.Println(err.Error(), "During opening file upload on backBlaze")
	}
	fmt.Println(name)
	myf, err2 := objectstore.Bucket.UploadFile(name, metadata, reader)
	reader.Close()
	if err2 != nil {
		fmt.Println(err.Error(), "During the file upload on backBlaze")
	} else {
		os.Remove(storedat)
	}
	url, _ = objectstore.Bucket.FileURL(myf.Name)
	fmt.Println(url, "is ther resposne")

	wd, _ := os.Getwd()
	fmt.Println(wd)
	fmt.Println(storedat)
	return url
}
