package config

import (
	"encoding/json"
	"os"
)

/* Config is structure that provides the configuration
|  parameters to the other parts of the app during the
|  run time
*/
var Config struct {
	AppUrl string `json:"AppUrl"`
	ViewPath    string `json:"ViewPath"`
	StoragePath string `json:"StoragePath"`
	PublicPath  string `json:"PublicPath"`
	Database    struct {
		DatabaseName string `json:"DatabaseName"`
		Driver       string `json:"Driver"`
		UserName     string `json:"UserName"`
		Password     string `json:"Password"`
	}
	Mail struct{
		Domain string `json:"Domain"`
		Key string `json:"Key"`
		PublicKey string `json:PublicKey"`
	}
}

/* Function that read the config.json file and populates
|  the Config singleton to use further in the app during
|  runtime.
*/
func LoadAppConfig() {
	//sync.Once{}.Do()
	current_dir, _ := os.Getwd()
	configFile, err := os.Open(current_dir + "/config/app.json")
	if err != nil {
		panic(err)
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&Config)
}
