package logger

import (
	"github.com/mchampaneri/imp/modules/config"
	"log"
	"os"
)

type Log struct {
}

/* Logger singleton that provides access to the every
|  logging functionality to the app. This will be
|  populated during the app initiation.
*/
var DefaultLogger Log

func (l *Log) Info(info string) {
	f, err := os.OpenFile(config.Config.StoragePath+"logger.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println("Info :", info)
}

func (l *Log) Warning(info string) {
	f, err := os.OpenFile(config.Config.StoragePath+"logger.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println("Warning :", info)
}

func (l *Log) Error(info string) {
	f, err := os.OpenFile(config.Config.StoragePath+"logger.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println("Error :", info)
}

func (l *Log) Track(info string) {
	f, err := os.OpenFile(config.Config.StoragePath+"logger.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println("Track :", info)
}
