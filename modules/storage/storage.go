package storage

import (
	"github.com/mchampaneri/imp/modules/config"
)

/*
  | Returns the path to the storage folder of the
  | app
*/
func StoragePath(path string) string {
	return config.Config.StoragePath + '/' + path
}
