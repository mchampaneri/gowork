package mailgun

import (
	"fmt"
	"github.com/mchampaneri/imp/modules/render"
		"github.com/mchampaneri/imp/modules/logger"
	"gopkg.in/mailgun/mailgun-go.v1"
)


var mg = mailgun.NewMailgun("puberstreet.com", "key-2fcff6a1835d0be445f2abb3f1c9adf5","pubkey-e9a9c8c8c9ae6aba14adc78121b425b0")


func SendMail(template, from, subject, info string, to string, data interface{}) {
	message := mailgun.NewMessage(
		from,
		subject,
		info,
		to)
	fmt.Println("sending the message")
	html := render.HtmlString(data, template)
	message.SetHtml(html)
	resp, id, err := mg.Send(message)
	if err != nil {
		logger.DefaultLogger.Error(err.Error())
	}
	fmt.Printf("ID: %s Resp: %s\n", id, resp)
}
