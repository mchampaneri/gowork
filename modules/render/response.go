package render

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/CloudyKit/jet"
	"github.com/gorilla/csrf"
	"github.com/mchampaneri/imp/modules/logger"
	"github.com/mchampaneri/imp/modules/session"
	"net/http"
	"os"
	"path/filepath"
	"github.com/mchampaneri/imp/modules/config"
)

/*
  | Render Package is intended to provide the functionality
  | that are necessary to render a response for any
  | in coming request
*/

var root, _ = os.Getwd()
var Jet = jet.NewHTMLSet(filepath.Join(root, "view"))

func init() {
	Jet.SetDevelopmentMode(true)
}

/*
 | Returns the data in form of "JSON" for the incoming
 | request
*/
func Json(w http.ResponseWriter, data interface{}) {
	response, err := json.Marshal(data)
	logger.DefaultLogger.Info("Json Rendered")
	if err != nil {
		logger.DefaultLogger.Error("Error " + err.Error() + " encountred during rendering Json Response")
	}
	fmt.Fprint(w, string(response))
}

/*
  | Returns a jet view in response of the in coming request
  | with the data supplied as parameter
*/
func View(w http.ResponseWriter, r *http.Request, data interface{}, viewName string) {
	session, err := session.UserSession.Get(r, "mvc-user-session")
	if err != nil || session.IsNew {
		fmt.Println("We failed to make the session")
	}
	templateName := viewName
	t, err := Jet.GetTemplate(templateName)
	if err != nil {
		logger.DefaultLogger.Error("Error " + err.Error() + " encountred during rendering View Response")
	}
	dataMap := make(map[string]interface{})
	if data != nil {
		dataMap = data.(map[string]interface{})
	}
	vars := make(jet.VarMap)
	// vars.Set("Auth", "true")
	if session.Values["auth"] == true {
		dataMap["AppUrl"] = config.Config.AppUrl
		dataMap["Auth"] = true
		dataMap["FirstName"] = session.Values["first_name"]
		dataMap["LastName"] = session.Values["last_name"]
		dataMap["NickName"] = session.Values["nickname"]
		dataMap["Email"] = session.Values["email"]
		dataMap["ProfilePic"] = session.Values["profile_pic"]
		if session.Values["active"] == true {
			dataMap["Active"] = session.Values["active"]
		}
	}

	dataMap["Message"] = session.Values["message"]
	dataMap["Token"] = csrf.Token(r)
	dataMap["Url"] = r.URL.Path

	// Resetting the Session Message
	session.Options.MaxAge = 0
	session.Values["message"] = nil
	session.Save(r, w)
	if err = t.Execute(w, vars, dataMap); err != nil {
		logger.DefaultLogger.Error("Error " + err.Error() + "encountred during executing View Render")
	}
}

func HtmlString(data interface{}, viewName string) string {
	var html bytes.Buffer
	templateName := viewName
	t, err := Jet.GetTemplate(templateName)
	if err != nil {
		logger.DefaultLogger.Error("Error " + err.Error() + "encountred during rendering View Response")
	}
	vars := make(jet.VarMap)
	if err = t.Execute(&html, vars, data); err != nil {
		logger.DefaultLogger.Error("Error " + err.Error() + "encountred during executing View Render")
	}
	return html.String()
}
