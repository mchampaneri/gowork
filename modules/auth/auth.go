package auth

import (
	"fmt"
	"github.com/mchampaneri/imp/model"
	"github.com/mchampaneri/imp/modules/database"
	"github.com/mchampaneri/imp/modules/session"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

/* Creating the Session Singleton that going
|  to used by the other parts of the app
*/

/* Authenticates the user against the credentials stored
| in the user table in the database and sets the session
| as well as a session cookie of the gorilla session for the
| further operations that requires the  authenticated
| User only.
*/

func Attempt(email string, password string, w http.ResponseWriter, req *http.Request) bool {
	var user = model.User{Email: email}
	has, err := database.Connection.Get(&user)
	if err != nil {
		fmt.Println(err.Error())
	}
	session, _ := session.UserSession.Get(req, "mvc-user-session")
	if has == true {
		if bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)) == nil {
			session.Values["id"] = user.Id
			session.Values["first_name"] = user.FirstName
			session.Values["email"] = user.Email
			session.Values["auth"] = true
			session.Values["role"] = user.Role
			session.Values["profile_pic"] = user.ProfilePic
			if user.Status == 1 {
				session.Values["active"] = true
				session.Values["message"] = "Welcome"
				session.Save(req, w)
				return true
			} else {
				session.Values["active"] = false
				session.Values["message"] = "Please Active Your Account by Verifying Your Email Address"
				session.Save(req, w)
				return true
			}
		} else {
			session.Values["auth"] = false
			session.Values["active"] = false
			session.Values["message"] = "Password Incorrect"
			session.Save(req, w)
			return false
		}

	} else {
		session.Values["auth"] = false
		session.Values["active"] = false
		session.Values["message"] = "Email Id Not Registred With Us"
		session.Save(req, w)
		return false
	}
	return false
}

/* Makes the check if the user is authenticate or not by
| checking the session is create for the user or not/
*/
func Check(req *http.Request) bool {
	session, err := session.UserSession.Get(req, "mvc-user-session")
	if (err != nil) || session.IsNew || (session.Values["auth"] != true) {
		fmt.Println("Either session is invalid or not exist")
		return false
	}
	return true
}

/* Clears the session of the user set by the gorilla session */
func Logout(req *http.Request, w http.ResponseWriter) bool {
	session, err := session.UserSession.Get(req, "mvc-user-session")
	if err == nil || !session.IsNew {
		for k := range session.Values {
			delete(session.Values, k)
		}
		fmt.Println("Logout Succesfully")
		return true
	}
	return false
}

func Auth(pass http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if Check(r) == true {
			pass(w, r)
			return
		}
		// redirect to login
		http.Redirect(w, r, "/", http.StatusMovedPermanently)
	}
}

func Admin(pass http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if Check(r) == true {
			session, _ := session.UserSession.Get(r, "mvc-user-session")
			if session.Values["role"] == 1 {
				pass(w, r)
				return
			}
		}
		// redirect to login
		http.Redirect(w, r, "/", http.StatusMovedPermanently)
	}
}
