package database

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"github.com/mchampaneri/imp/modules/config"
	"github.com/mchampaneri/imp/modules/logger"
)

/* Database connection singleton to provide access
|  to the database and xorm.
|  MVC Uses xorm for the database related works
*/
var Connection *xorm.Engine

/* GetConnections function is responsible for opening
|  and manging the connection by populating the connection
|  singleton for the further processing
*/
func GetConnection() *xorm.Engine {
	Engine, db_connect_failed := xorm.NewEngine(config.Config.Database.Driver, config.Config.Database.UserName+":"+config.Config.Database.Password+"@/"+config.Config.Database.DatabaseName+"?charset=utf8")
	//Engine.SetMapper(core.PluralizeMapper{})
	if db_connect_failed != nil {
		logger.DefaultLogger.Error("Xorm Engine failed to connect to the database")
	}
	return Engine
}
