package search

import (
	"github.com/blevesearch/bleve"
	"github.com/mchampaneri/imp/modules/logger"
)

var UserIndex, PublicationIndex, PageIndex  bleve.Index
var UserErr, PublicationErr, PageErr error

func LoadIndex() {
	UserIndex, UserErr = bleve.Open("users.bleve")
	PublicationIndex, PublicationErr = bleve.Open("publications.bleve")
	PageIndex, PageErr = bleve.Open("pages.bleve")
	// if doesn't exists or something goes wrong...
	if UserErr != nil {
		// create a new mapping file and create a new index
		mapping := bleve.NewIndexMapping()
		UserIndex, UserErr = bleve.New("users.bleve", mapping)
		if UserErr != nil {
			return
		}
	}
	if PublicationErr != nil {
		// create a new mapping file and create a new index
		publication := bleve.NewDocumentMapping()

		publicationCoverImage := bleve.NewTextFieldMapping()
		publicationCoverImage.Store = true
		publication.AddFieldMappingsAt("CoverImage",publicationCoverImage)

		publicationId := bleve.NewNumericFieldMapping()
		publicationId.Store = true
		publication.AddFieldMappingsAt("Id", publicationId)

		publicationTags := bleve.NewTextFieldMapping()
		publicationTags.Store = true
		publication.AddFieldMappingsAt("Tags", publicationTags)

		publicationTitle := bleve.NewTextFieldMapping()
		publicationTitle.Store = true
		publication.AddFieldMappingsAt("Title", publicationTitle)

		mapping := bleve.NewIndexMapping()
		mapping.AddDocumentMapping("publication",publication)
		mapping.DefaultMapping = publication
		PublicationIndex, PublicationErr = bleve.New("publications.bleve", mapping)
		if UserErr != nil {
			return
		}
	}
	if PageErr != nil {
		// create a new mapping file and create a new index
		mapping := bleve.NewIndexMapping()
		PageIndex, PageErr = bleve.New("pages.bleve", mapping)
		if UserErr != nil {
			return
		}
	}
}

func IndexUser(as string, data interface{}) {
	err := UserIndex.Index(as, data)
	if err!=nil{
		logger.DefaultLogger.Error("Search Module : IndexUser() "+err.Error())
	}
}

func FindUser(part string) string {
	query := bleve.NewMatchPhraseQuery(part)
	search := bleve.NewSearchRequest(query)
	search.Highlight = bleve.NewHighlight()
	searchResults, err := UserIndex.Search(search)
	if err != nil {
		logger.DefaultLogger.Error("Search Module : FindUser() "+err.Error())
	}
	return searchResults.String()
}

func IndexPublication(as string, data interface{}) {
	err := PublicationIndex.Index(as, data)
	logger.DefaultLogger.Info("Indexing the Entry")
	if err!=nil{
		logger.DefaultLogger.Error("Search Module : IndexPublication() "+err.Error())
	}
}

func UnIndexPublication(as string) {
	err := PublicationIndex.Delete(as)
	logger.DefaultLogger.Info("Indexing the Entry")
	if err!=nil{
		logger.DefaultLogger.Error("Search Module : IndexPublication() "+err.Error())
	}
}

func FindPublication(part string)  *bleve.SearchResult {
	query := bleve.NewMatchPhraseQuery(part)
	search := bleve.NewSearchRequest(query)
	search.Fields = []string{"id","title","cover_image"}
	search.Highlight = bleve.NewHighlight()
	searchResult, err := PublicationIndex.Search(search)
	if err != nil {
		logger.DefaultLogger.Error("Search Module : FindPublication() " + err.Error())
	}
	return searchResult
}

func IndexPage(as string, data interface{}) {
	err := PageIndex.Index(as, data)
	if err!=nil{
		logger.DefaultLogger.Error("Search Module : IndexPage() "+err.Error())
	}
}

func FindPage(part string) string {
	query := bleve.NewMatchPhraseQuery(part)
	search := bleve.NewSearchRequest(query)
	search.Highlight = bleve.NewHighlight()
	searchResults, err := PageIndex.Search(search)
	if err != nil {
		logger.DefaultLogger.Error("Search Module : FindPages() "+err.Error())
	}
	return searchResults.String()
}