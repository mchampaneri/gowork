package validator

import "github.com/asaskevich/govalidator"

/*
  | Singleton initialization of the go validator
  | It is used to validate the data on the
  | go structs
*/
func Init() {
	govalidator.SetFieldsRequiredByDefault(false)
}
