package routes

import (
	"fmt"
	"github.com/gorilla/csrf"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/mchampaneri/imp/modules/render"
	"net/http"
)

/* *
 *  Registering the routes for the web
 */
func RegisterWebRoutes() {

	routes := mux.NewRouter()

	/////////////////////////////////////////////////////////////////////////////////////////////
	////			 Opening the public directory for the open assets                        ////
	//// 			 Don't Change / Remove this line unless you know  		                 ////
	//// 			 what are you doing .					    		                     ////
	/////////////////////////////////////////////////////////////////////////////////////////////

	routes.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))))
	routes.PathPrefix("/storage/").Handler(http.StripPrefix("/storage/", http.FileServer(http.Dir("./storage"))))

	/////////////////////////////////////////////////////////////////////////////////////////////
	////			 Define you routes here							                         ////
	/////////////////////////////////////////////////////////////////////////////////////////////

	routes.HandleFunc("/favicon.ico", faviconHandler)

	/////////////////////////////////////////////////////////////////////////////////////////////
	////			Opening Port 8085 (web) 					           ////
	/////////////////////////////////////////////////////////////////////////////////////////////

	routes.NotFoundHandler = http.HandlerFunc(notFoundHandle)

	http.Handle("/", routes)

	fmt.Println("Web Routes Opened on port : 8085")

	http.ListenAndServe(":8085", handlers.CompressHandler(csrf.Protect([]byte("El0a6L8uqv"), csrf.Secure(false))(routes)))

}

func notFoundHandle(w http.ResponseWriter, r *http.Request) {
	render.View(w, r, nil, "errors/404.html")
}
func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./favicon.ico")
}
