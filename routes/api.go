package routes

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/gorilla/mux"
	"github.com/mchampaneri/imp/controller"
	"golang.org/x/net/context"
	"net/http"
	"time"
)

var mySigningKey = []byte("1234abcd")

func RegisterApiRoutes() {

	api := mux.NewRouter()
	api_router := api.PathPrefix("/api").Subrouter()

	/////////////////////////////////////////////////////////////////////////////////////////////
	////			Define you routes here							//////
	/////////////////////////////////////////////////////////////////////////////////////////////

	api_router.HandleFunc("/get-token", controller.Apiv1.GetToken).Methods("POST")

	api_router.HandleFunc("/get-user", jwtCheck(controller.Apiv1.GetUser)).Methods("GET")

	api_router.HandleFunc("/register-user", controller.Apiv1.RegisterUser).Methods("POST")

	/////////////////////////////////////////////////////////////////////////////////////////////
	////			Opening Port 12345 (api) 					           ////
	/////////////////////////////////////////////////////////////////////////////////////////////
	http.Handle("/api_router", api_router)
	fmt.Println("Api Routes Opened on port : 12345")
	http.ListenAndServe(":12345", api_router)
}

func jwtCheck(pass http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		info, _ := request.ParseFromRequest(r, request.AuthorizationHeaderExtractor, func(token *jwt.Token) (interface{}, error) {
			// since we only use the one private key to sign the tokens,
			// we also only use its public counter part to verify
			return mySigningKey, nil
		})
		key := info.Raw
		claim, _ := jwt.Parse(string(key[6:]), func(token *jwt.Token) (interface{}, error) {
			// since we only use the one private key to sign the tokens,
			// we also only use its public counter part to verify
			return mySigningKey, nil
		})
		resp := make(map[string]interface{})
		resp["message"] = "succesfull"
		var abc jwt.MapClaims = claim.Claims.(jwt.MapClaims)
		resp["data"] = abc["exp"]
		resp["id"] = abc["jti"]
		//panic(reflect.TypeOf(resp["data"]))

		if abc.VerifyExpiresAt(time.Now().UnixNano(), true) && abc.VerifyIssuer("Puber-Security", true) {
			fmt.Println("Setp-1")
			ctx := context.WithValue(r.Context(), "Id", abc["jti"])
			fmt.Println("Setp-2", abc["jti"])
			pass(w, r.WithContext(ctx))
			fmt.Println("Setp-3")
			return
		}
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
	}

}
