
package model

import "time"

type PasswordReset struct{
	Id       int    `json:"id" xorm:"int(12) not null unique pk 'id'"   `
	UserId   int    `json:"user_id" xorm:"int(12) not null 'user_id'"   `
	Token    string `json:"token" xorm:"varchar(60) not null unique  'token'"   `

	CreatedAt time.Time `json:"created_at" xorm:"timestamp 'created_at'" `
}