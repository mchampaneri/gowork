package model

type  Read struct {
	Id            int    `json:"id" xorm:"int(12) not null unique pk 'id'"  `
	UserId        int      `json:"user_id" xorm:"int(10)  'user_id'  "`
	PublicationId int        `json:"publication_id"  xorm:"int(10)  'publication_id'  "`
	PageId        int        `json:"page_id"  xorm:"int(10)  'page_id'  "`
	Offset        float64 `json:"offset"  xorm:"int(10)  'offset'  "`
}
