package model

import (
	"golang.org/x/crypto/bcrypt"
	"time"
)

type User struct {
	Id          int       `json:"id" xorm:"int(12) not null unique pk 'id'"   `
	Role        int       `json:"role" xorm:"int(12) not null  'role'"   `
	FirstName   string    `json:"first_name"  xorm:"varchar(25) not null  'first_name' " valid:"required"`
	LastName    string    `json:"last_name"  xorm:"varchar(25) not null  'last_name' " valid:"required"`
	NickName    string    `json:"nickname" xorm:"varchar(25) 'nickname' "`
	Gender      int       `json:"gender" xorm:"int(12) 'gender' " `
	Dob         time.Time `json:"dob" xorm:"Date 'dob' "`
	Email       string    `json:"email" xorm:"varchar(25) not null unique 'email' "  valid:"required,email"`
	Password    string    `json:"password" xorm:"varchar(25) not null 'password' "  valid:"required"`
	Status      int       `json:"id" xorm:"int(2) not null default false 'status' " `
	ProfilePic  string    `json:"profile_pic" xorm:"varchar(100) 'profile_pic' "`

	Nationality string `json:"nationality" xorm:"varchar(60) 'nationality' "`
	Language    string `json:"language" xorm:"varchar(60) 'language' "`
	Education   string `json:"education" xorm:"varchar(60) 'education' "`
	Interest    string `json:"interest" xorm:"varchar(60) 'interest' "`
	Experience  string `json:"experience" xorm:"varchar(60) 'experience' "`
	Achievement string `json:"achievement" xorm:"varchar(60) 'achievement' "`
	Occupation  string `json:"occupation" xorm:"varchar(60) 'occupation' "`
	Married     int    `json:"married" xorm:"varchar(60) 'married' "`
	Slug        string `json:"slug" xorm:"varchar(100) 'slug'`
	Phone       string `json:"phone" xorm:"string(15)  'phone' "`
	Address     string `json:"address" xorm:"string(100) 'address' "`

	CreatedAt   time.Time `json:"created_at" xorm:"timestamp 'created_at'" `
	UpdatedAt   time.Time `json:"updated_at" xorm:"timestamp 'updated_at'" `
	DeletedAt   time.Time `json:"deleted_at" xorm:"timestamp 'deleted_at'" `
}

func (User) HashPassword(password string) string {
	hashBytes, _ := bcrypt.GenerateFromPassword([]byte(password), 10)
	return string(hashBytes)
}
