package model

import "time"

type Resource struct {
	Id       int    `json:"id" xorm:"int(12) not null unique pk 'id'"   `
	Type     string `json:"type"  xorm:"varchar(25) not null  'type' " valid:"required"`
	Value    string `json:"value" xorm:"varchar(25) 'value' "`

	CreatedAt time.Time `json:"created_at" xorm:"timestamp 'created_at'" `
	UpdatedAt time.Time `json:"updated_at" xorm:"timestamp 'updated_at'" `
	DeletedAt time.Time `json:"deleted_at" xorm:"timestamp 'deleted_at'" `
}