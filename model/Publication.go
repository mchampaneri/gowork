package model

import (
	"encoding/json"
	"time"
)

type Publication struct {
	Id         int    `json:"id" xorm:"int(12) not null unique pk 'id' "`
	Title      string `json:"title"  xorm:"varchar(25)   'title' "`
	UserId     int    `json:"user_id" xorm:"int(10)  'user_id'  "`
	Pages      []Page `json:"pages" xorm:"-"`
	Tags       string `json:"tags" xorm:"text 'tags'"`
	CoverImage string `json:"cover_image" xorm:"text 'cover_image' "`
	BackImage  string `json:"back_image" xorm:"-" `
	Visibility int    `json:"visibility" xorm:"-"`
	Type       int    `json:"type" xorm:"int(2) not null 'type' "`

	Published int `json:"published" xorm:"int(2) 'published' "`

	Slug string `json:"slug" xorm:"varchar(100) 'slug'"`
	Read int    `json:"read" xorm:"-" `

	CreatedAt time.Time `json:"created_at" xorm:"timestamp 'created_at'" `
	UpdatedAt time.Time `json:"updated_at" xorm:"timestamp 'updated_at'" `
	DeletedAt time.Time `json:"deleted_at" xorm:"timestamp 'deleted_at'" `
}

func (u *Publication) MarshalJSON() ([]byte, error) {
	type Alias Publication
	return json.Marshal(&struct {
		CreatedAt string `json:"created_at"`
		*Alias
	}{
		CreatedAt: u.CreatedAt.Format("2 Jan ,2006"),
		Alias:     (*Alias)(u),
	})
}

type Page struct {
	Id            int    `json:"id" xorm:"int(12) not null unique pk 'id' "`
	PublicationId int    `json:"publication_id" xorm:"int(12) 'publication_id' "`
	Title         string `json:"title"  xorm:"varchar(25) not null  'title' "`
	Content       string `json:"content"  xorm:"varchar(60) 'content' "`
	UserId        int    `json:"user_id" xorm:"int(10) not null  'user_id'  "`
	Sequence      int    `json:"sequence" xorm:"int(10) 'sequence'" `

	CreatedAt time.Time `json:"created_at" xorm:"timestamp 'created_at'" `
	UpdatedAt time.Time `json:"updated_at" xorm:"timestamp 'updated_at'" `
	DeletedAt time.Time `json:"deleted_at" xorm:"timestamp 'deleted_at'" `
}
