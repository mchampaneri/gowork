package main

import (
	"bufio"
	"fmt"
	"github.com/mchampaneri/mvc/internals"
	"os"
	"os/exec"
)

func main() {

	logger := modules.Log{}
	var line []byte
	var err error
	dependency_register, err := os.OpenFile("./../packages.bat", os.O_RDONLY, 0666)
	if err != nil {
		logger.Error("Can not read the packages.bat file")
	} else {
		logger.Info("Packages.txt open successfully ")
	}

	file := bufio.NewReader(dependency_register)
	for {
		line, _, err = file.ReadLine()

		if err != nil {
			fmt.Println("we got a stop")
			fmt.Println(err.Error())
			break
		}
		command := exec.Command("cmd", "/C", "go", "get", string(line))
		err = command.Start()
		if err != nil {
			fmt.Println(err.Error())
		}
		fmt.Println(string(line))

	}
}
